const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", printName);

txtLastName.addEventListener("keyup", printName);

function printName(event) {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}
